<?php
/**
 * @file
 * Colored theme settings implementation.
 */

/**
 * Colored theme settings. Add custom settings in subtheme theme-settings file.
 */
function colored_form_system_theme_settings_alter(&$form, $form_state) {

  $form['colored_info'] = array(
    '#prefix' => '<h3>Colored Theme Settings:</h3> ',
    '#weight' => -40,
  );
